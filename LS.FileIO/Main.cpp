#include <iostream>
#include <conio.h>
#include <fstream>
#include <string>

using namespace std;


void PrintNums(int *pArray, int size, ostream &os) 
{
	for (int i = 0; i < size; i++)
	{
		os << pArray[1] << "\n";
	}
}

int main() 
{
	/*
	cout << "Enter your name: ";
	string name;
	// cin >> name; // only gets to the first whitespace character
	getline(cin, name);

	cout << "Your name is: " << name << ".\n";*/

	const int SIZE = 3;
	int numbers[SIZE];

	for (int i = 0; i < SIZE; i++)
	{
		cout << (i + 1) << ": ";
		cin >> numbers[i];
	}

	PrintNums(numbers, SIZE, cout);

	const string filepath = "C:\\Temp\\Test.txt";


	//ofstream ofs;
	//ofs.open(filepath);
	ofstream ofs(filepath); //for printing just one file

	/*cout << "Your numbers are:\n";
	for (int i = 0; i < SIZE; i++)
	{
		ofs << numbers[i] << "\n";
	}*/

	PrintNums(numbers, SIZE, ofs);

	ofs.close();



	cout << "Reading the file:\n";

	ifstream ifs(filepath);

	string line;
	while (getline(ifs, line)) 
	{
		cout << line << "\n";
	}

	ifs.close();



	_getch();
	return 0;
}